﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ChrisXue.GameEngine
{
    public class Camera : GameComponent
    {
        public Vector3 Position { get; set; }
        public Quaternion Orientation { get; set; }

        public float ZNear { get; set; }
        public float ZFar { get; set; }

        public Matrix View { get; set; }
        public Matrix Projection { get; set; }

        public override void Update()
        {
            base.Update();
        }
    }
}
