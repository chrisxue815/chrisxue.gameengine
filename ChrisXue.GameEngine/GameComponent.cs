﻿namespace ChrisXue.GameEngine
{
    public class GameComponent
    {
        protected BaseGame Game { get; set; }

        public GameComponent()
        {
            Game = BaseGame.Instance;
        }

        public virtual void Initialize()
        {
        }

        public virtual void LoadContent()
        {
        }

        public virtual void UnloadContent()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }
    }
}
